tgNamespace.tgContainer.registerInnerGate("log", function (oTrackGate,sLocalConfData)
{
    this.oTrackGate=oTrackGate;
    this.sLocalConfData=sLocalConfData;
	this.iLog = 0;
	this.oLog = {};
	this.bPageLoad = false;

    this._construct = function ()
    {
	    this.oTrackGate.registerInnerGateFunctions("log","_all_",this.logAllCmd);
	    this.oTrackGate.registerInnerGateFunctions("log","final",this.logPageLoad);
    }


    this.logAllCmd = function (oFuncArg, sFuncName)
    {
		if(sFuncName !== "settimepoint") {
			var oTmpLog = {};

			oTmpLog["Function"] = sFuncName;

			for(var sKey in oFuncArg)
			{
				var xVal = oFuncArg[sKey];

				if(typeof xVal === "object")
				{
					var sConsoleValue = JSON.stringify(xVal);
				}
				else
				{
					var sConsoleValue = xVal
				}

				oTmpLog[sKey] = (typeof xVal)+" : "+sConsoleValue;
			}

			this.oLog[this.iLog] = oTmpLog;
			this.iLog++;

			//Wenn der Pageload bereits statt gefunden hat, dann gleich alles loggen.
			if(this.bPageLoad === true)
			{
				this.logPageLoad();
			}
		}
    }

	this.logPageLoad = function ()
	{
		if(
			this.oTrackGate.bTestMode === true &&
			this.oTrackGate.bLogMode === true
		)
		{
			console.log("+++++++ TrackGate - Debug Output +++++++\n\n");
			for(var iCounter in this.oLog) {
				var sFunction = this.oLog[iCounter]["Function"];
				delete this.oLog[iCounter]["Function"];
				console.log("\n\n");
				var oTempJson = {};
				oTempJson[sFunction] = this.oLog[iCounter];
				if(typeof console.table !== "undefined")
				{
                    console.table(oTempJson);
				}
				else
				{
                    console.log(oTempJson);
				}

				console.log(this.oLog[iCounter]);
			}
			this.bPageLoad = true;
			this.oLog = {};
		}
	}
});

//Config fuer den GTM TrackGate registrieren (diese ist bereits im Einbaucode definiert)
tgNamespace.tgContainer.registerInnerConfig("log",{
	countConfigs: 1,
	countFuncLibs: 0
}, {});